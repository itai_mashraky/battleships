using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Simple_Node : MonoBehaviour
{

    [SerializeField] private AI_Controller AI_Controller_Script;
    [SerializeField] private Material NodeMaterial;

    // Start is called before the first frame update
    void Start()
    {
        AI_Controller_Script = GameObject.Find("AI_Controller").GetComponent<AI_Controller>();
        NodeMaterial = gameObject.GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnMouseDown()
    {
        AI_Controller_Script.RegisterHit(this);
        gameObject.GetComponent<BoxCollider>().enabled = false;
    }

    public void SetColor(Color color)
    {

        NodeMaterial.color = color;
    }
}
