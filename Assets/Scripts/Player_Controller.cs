using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class Player_Controller : MonoBehaviour
{

    public enum directions
    {
        down,   // 0
        up,     // 1
        left,   // 2
        right   // 3

    }


    [SerializeField] private Simple_Node[,] AI_Map;
    [SerializeField] private bool CanChooseNode, CanDeployShip;
    [SerializeField] private Node[,] PlayerMap;
    [SerializeField] private Transform StartPos;
    private int curShipSize, curShipIndex;
    private directions activeDirction;
    [SerializeField] private GameObject[] Ships;
    [SerializeField] private GameObject LastButton;
    [SerializeField] private int counter;

    // Start is called before the first frame update
    void Start()
    {

        GameObject[] Nodes = GameObject.FindGameObjectsWithTag("Node");
        GameObject[] SimpleNodes = GameObject.FindGameObjectsWithTag("Simple_Node");
        Ships = GameObject.FindGameObjectsWithTag("Ship");


        Array.Sort(Nodes, SortByName);
        Array.Sort(SimpleNodes, SortByName);
        fillArray(Nodes, SimpleNodes);
        CanDeployShip = false;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public int GetShipSize()
    {
        return curShipSize;
    }
    public void EnableShip(int size)
    {
        LastButton = EventSystem.current.currentSelectedGameObject;
        SetCanChooseNode(true);

        for (int i = 0; i < Ships.Length; i++)
        {
            if (size == Ships[i].GetComponent<ShipCode>().getSize() && !Ships[i].GetComponent<ShipCode>().GetIsDeployed())
            {
                curShipIndex = i;
                Ships[i].GetComponent<ShipCode>().SetIsDeployed(true);
                break;
            }
        }
        curShipSize = size;
    }
    int SortByName(GameObject a, GameObject b)
    {
        return a.transform.name.CompareTo(b.transform.name);
    }
    public bool GetCanChooseNode()
    {
        return CanChooseNode;
    }
    public void SetCanChooseNode(bool value)
    {
        CanChooseNode = value;
    }
    public int Test(String NodeName)
    {

        int x, y, valueToReturn = 0;
        x = NodeName[NodeName.Length - 2] - '0';
        y = NodeName[NodeName.Length - 1] - '0';
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            activeDirction = directions.down;

            if (x + curShipSize - 1 < 10)
            {
                valueToReturn = testRow(x, y, curShipSize, 0) ? 2 : 1;
            }
            else
            {
                valueToReturn = 1;
            }
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            activeDirction = directions.up;
            if (x - curShipSize + 1 >= 0)
            {
                valueToReturn = testRow(x, y, curShipSize, 1) ? 2 : 1;
            }
            else
            {
                valueToReturn = 1;
            }

        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            activeDirction = directions.left;

            if (y - curShipSize + 1 >= 0)
            {
                valueToReturn = testRow(x, y, curShipSize, 2) ? 2 : 1;
            }
            else
            {
                valueToReturn = 1;
            }

        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            activeDirction = directions.right;

            if (y + curShipSize - 1 < 10)
            {
                valueToReturn = testRow(x, y, curShipSize, 3) ? 2 : 1;
            }
            else
            {
                valueToReturn = 1;
            }
        }


        return valueToReturn;

    }
    public bool testRow(int x, int y, int size, int index)
    {
        if (size > 0)
        {
            switch (index)
            {
                case 0:
                    return PlayerMap[x, y].GetIsEmpty() && testRow(x + 1, y, size - 1, index);
                case 1:
                    return PlayerMap[x, y].GetIsEmpty() && testRow(x - 1, y, size - 1, index);
                case 2:
                    return PlayerMap[x, y].GetIsEmpty() && testRow(x, y - 1, size - 1, index);
                case 3:
                    return PlayerMap[x, y].GetIsEmpty() && testRow(x, y + 1, size - 1, index);
            }
        }
        return true;
    }
    void fillArray(GameObject[] Nodes, GameObject[] SimpleNodes)
    {

        PlayerMap = new Node[10, 10];
        AI_Map = new Simple_Node[10, 10];
        int temp = 0;

        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                PlayerMap[i, j] = Nodes[temp].GetComponent<Node>();
                AI_Map[i, j] = SimpleNodes[temp].GetComponent<Simple_Node>();
                temp++;
            }
        }
    }
    public void DeployShip(String NodeName)
    {
        int x, y, index, offset;

        x = NodeName[NodeName.Length - 2] - '0';
        y = NodeName[NodeName.Length - 1] - '0';

        Transform temp = Ships[curShipIndex].transform;

        temp.position = PlayerMap[x, y].transform.position;

        index = (int)activeDirction;

        if (curShipSize == 2)
        {
            offset = 1;
        }
        else
        {
            offset = -1;
        }

        switch (index)
        {
            case 0:
                temp.rotation = Quaternion.Euler(0, 0, 0);
                temp.position = new Vector3(temp.position.x, temp.position.y + 1, temp.position.z - curShipSize + 1);

                break;
            case 1:
                temp.rotation = Quaternion.Euler(0, 0, 0);
                temp.position = new Vector3(temp.position.x, temp.position.y + 1, temp.position.z + curShipSize - 1);
                break;
            case 2:
                temp.rotation = Quaternion.Euler(0, 90, 0);
                temp.position = new Vector3(temp.position.x - curShipSize + 1, temp.position.y + 1, temp.position.z);
                break;
            case 3:
                temp.rotation = Quaternion.Euler(0, 90, 0);
                temp.position = new Vector3(temp.position.x + curShipSize - 1, temp.position.y + 1, temp.position.z);
                break;
        }
        CanDeployShip = true;
    }
    public void SetShip(String NodeName)
    {

        if (CanDeployShip)
        {
            int x, y, index;
            index = (int)activeDirction;
            x = NodeName[NodeName.Length - 2] - '0';
            y = NodeName[NodeName.Length - 1] - '0';
            CanChooseNode = false;
            CanDeployShip = false;
            SetRow(x, y, curShipSize, index);
            LastButton.SetActive(false);
            counter++;
        }
    }
    public void SetRow(int x, int y, int size, int index)
    {
        if (size > 0)
        {
            switch (index)
            {
                case 0:
                    PlayerMap[x, y].SetIsEmpty(false);
                    SetRow(x + 1, y, size - 1, index);
                    break;
                case 1:
                    PlayerMap[x, y].SetIsEmpty(false);

                    SetRow(x - 1, y, size - 1, index);
                    break;
                case 2:
                    PlayerMap[x, y].SetIsEmpty(false);

                    SetRow(x, y - 1, size - 1, index);
                    break;
                case 3:
                    PlayerMap[x, y].SetIsEmpty(false);
                    SetRow(x, y + 1, size - 1, index);
                    break;
            }
        }
    }
    public void ResetShip()
    {
        Ships[curShipIndex].transform.position = Ships[curShipIndex].GetComponent<ShipCode>().GetLastPos();
    }
    public int GetCounter()
    {
        return counter;
    }
    public Node[,] GetMap()
    {
        return PlayerMap;
    }
}

