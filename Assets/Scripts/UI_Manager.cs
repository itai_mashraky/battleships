using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_Manager : MonoBehaviour
{
    [SerializeField] private Player_Controller Player_ControllerScript;
    [SerializeField] private GameObject LoseMenu;
    [SerializeField] private GameObject WinMenu;
    [SerializeField] private GameObject PauseMenu;


    // Start is called before the first frame update
    void Start()
    {
        Player_ControllerScript = GameObject.Find("Player_Controller").GetComponent<Player_Controller>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (!PauseMenu.activeSelf)
            {
                PauseMenu.SetActive(true);
                Time.timeScale = 0;
            }
            else
            {
                PauseMenu.SetActive(false);
                Time.timeScale = 1;

            }
        }
    }

    public void ResumeGame()
    {
        PauseMenu.SetActive(false);
        Time.timeScale = 1;
    }

    public void LoseGame()
    {
        Time.timeScale = 0;

        LoseMenu.SetActive(true);
    }

    public void WinGame()
    {
        Time.timeScale = 0;

        WinMenu.SetActive(true);
    }

    public void BuildShip(int size)
    {
        Player_ControllerScript.EnableShip(size);
    }

    public void RestartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu()
    {
        Time.timeScale = 1;

        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
        print("CLose game");
    }

    public void ChangeCameraToPlayer()
    {
        Camera.main.transform.rotation = Camera.main.transform.rotation = Quaternion.Euler(60,0,0);

    }
    public void ChangeCameraToEnemy()
    {
        Camera.main.transform.rotation = Camera.main.transform.rotation = Quaternion.Euler(0, 0, 0);

    }
}
